
def getEntities():
    return [
        {
            'name': 'region',
            'url': 'https://b2c.cpost.cz/services/Address/getRegionListAsJson',
        },
        {
            'name': 'district', 'parent': 'region',
            'url': 'https://b2c.cpost.cz/services/Address/getDistrictListAsJson',
            'searchParameter': 'id'},
        {
            'name': 'city', 'parent': 'district',
            'url': 'https://b2c.cpost.cz/services/Address/getCityListAsJson',
            'searchParameter': 'id'
        },
        {
            'name': 'citypart', 'parent': 'city',
            'url': 'https://b2c.cpost.cz/services/Address/getCityPartListAsJson',
            'searchParameter': 'id'
        },
        {
            'name': 'street', 'parent': 'cityPart',
            'url': 'https://b2c.cpost.cz/services/Address/getStreetListAsJson',
            'searchParameter': 'id'
        },
        {
            'name': 'number', 'parent': 'street',
            'url': 'https://b2c.cpost.cz/services/Address/getNumberListAsJson',
            'searchParameter': 'idStreet'
        }
    ]
