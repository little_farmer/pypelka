---
marp: true
theme: uncover
class: modern
paginate: true
---

<style>
    :root {
        --color-background: #282828;
        --color-background-code: #181818;
        --color-foreground: #e8e8e8;
    }
  
    a {
        color: #7cafc2;
    }

    h1 {
        font-size: 90px;
        color: #ab4642;
    }
</style>

![bg left:35% 80%](https://gitlab.com/uploads/-/system/project/avatar/44384632/pypelka.jpg)

# Pypelka :peanuts:

How i m!%@#$%d api of
Czech Post and you can too!

[gitlab.com/little_farmer/pypelka](https://gitlab.com/little_farmer/pypelka)

---

# What is it and why

- API documentation - [b2c.cpost.cz](https://b2c.cpost.cz/)
- you can't download all data at once
  (at least I didn't find it)
- concurrently fetching data
- can return to downloading from last point
- !fun fact statistics
- 53.7 MiB database size

---

# Some SQL(ite) Schema

![](./db-schema.png)

---

# Some progressbar:pinched_fingers:

```
Starting downloading: region
Downloaded: 14

Starting downloading: district
[0:00:00- Downloading ] |******************| [ 14/14 ] (ETA:  00:00:00)
Iterated region: 14
Downloaded: 14
district in DB: 77

Starting downloading: city
[0:00:19- Downloading ] |******************| [ 77/77 ] (ETA:  00:00:00)
Iterated district: 77
Downloaded: 77
city in DB: 6258

Starting downloading: citypart
[0:00:06- Downloading ] |*              | [ 250/6258 ] (ETA:   0:02:43)
```

---

# Some of your own decisions!

```
Make your choice!
c  - Get the count of all items
mh - Get highest house numbers
mo - Get highest orientation numbers
me - Get highest evidence numbers
s  - Get all streets for a given name
si - Get single street numbers by ID -> use s
sn - Get streets with all their numbers by name
q  -  Quit :(
```

---

# Some results :scroll:

```
COUNT OF ITEMS
+----------+-----------+
| Entity   |     Count |
+----------+-----------+
| region   |        14 |
| district |        77 |
| city     |     6,258 |
| citypart |    15,105 |
| street   |    84,892 |
| number   | 1,442,506 |
| SUM      | 1,548,852 |
+----------+-----------+
```

---

# Some more results :shrimp:

```
What should if do? s
Give me name of street: václavské náměstí

RESULT FOR: VÁCLAVSKÉ NÁMĚSTÍ
+--------------------+--------------------+-------------------+--------------------------+-------------------+-------+
|       Region       |      District      |        City       |        City Part         |       Street      |   ID  |
+--------------------+--------------------+-------------------+--------------------------+-------------------+-------+
| Hlavní město Praha | Hlavní město Praha |       Praha       |        Nové Město        | Václavské náměstí | 63390 |
|      Ústecký       |     Litoměřice     |     Litoměřice    |        Předměstí         | Václavské náměstí | 63397 |
|      Ústecký       |     Litoměřice     |      Lovosice     |         Lovosice         | Václavské náměstí | 63398 |
|      Ústecký       |       Louny        |      Černčice     |         Černčice         | Václavské náměstí | 63393 |
|      Ústecký       |   Ústí nad Labem   |       Trmice      |          Trmice          | Václavské náměstí | 63402 |
|    Jihomoravský    |       Znojmo       |       Znojmo      |          Znojmo          | Václavské náměstí | 63405 |
|  Královéhradecký   |       Jičín        |   Vysoké Veselí   |      Vysoké Veselí       | Václavské náměstí | 63404 |
|    Středočeský     |       Kolín        |     Nová Ves I    |        Nová Ves I        | Václavské náměstí | 63399 |
|    Středočeský     |     Kutná Hora     |     Kutná Hora    | Kutná Hora-Vnitřní Město | Václavské náměstí | 63395 |
|    Středočeský     |     Kutná Hora     | Uhlířské Janovice |    Uhlířské Janovice     | Václavské náměstí | 63403 |
|    Středočeský     |       Kladno       |       Kladno      |        Kročehlavy        | Václavské náměstí | 80434 |
|    Středočeský     |      Rakovník      |      Kněževes     |         Kněževes         | Václavské náměstí | 63394 |
|    Středočeský     |      Příbram       |      Příbram      |        Příbram II        | Václavské náměstí | 63401 |
|     Pardubický     |  Ústí nad Orlicí   |      Letohrad     |         Letohrad         | Václavské náměstí | 63396 |
|     Pardubický     |     Pardubice      |      Přelouč      |         Přelouč          | Václavské náměstí | 63400 |
+--------------------+--------------------+-------------------+--------------------------+-------------------+-------+
```

---

# Some questions :snake:

---

Thank you, bye!
