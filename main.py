#!/usr/bin/env python

import argparse
import data.entities as entities
import services.dbConnect as dbConnect
import services.postDownloader as postDownloader
import scripts.aggregations as aggregations
import scripts.veryImportant as veryImportant

parser = argparse.ArgumentParser(prog='python main.py',
                                 description='Pypelka, your personal downloader of cpost',
                                 epilog='Use it wisely!')
parser.add_argument('-d', '--database', dest='db',
                    type=str, help='Name of database')

args = parser.parse_args()

dbName = 'post'
if args.db is not None:
    dbName = args.db

# get cursor and connection object
cur, conn = dbConnect.getCurConn(dbName)

# get entities
entities = entities.getEntities()

# download data if not fully downloaded
postDownloader.postDownloader(cur, conn, entities)

while True:
    print('Make your choice!')
    print('c  - Get the count of all items')
    print('mh - Get highest house numbers')
    print('mo - Get highest orientation numbers')
    print('me - Get highest evidence numbers')
    print('s  - Get all streets for a given name')
    print('si - Get single street numbers by ID -> use s')
    print('sn - Get streets with all their numbers by name')
    print('q  - Quit :(')

    userInput = input('What should if do? ')

    match userInput:
        case 'c':
            aggregations.countofItem(cur, entities)
        case 'mh':
            aggregations.getMaxHouseNumber(cur)
        case 'mo':
            aggregations.getMaxOrientationNumber(cur)
        case 'me':
            aggregations.getMaxEvidenceNumber(cur)
        case 's':
            subInput = input('Give me name of street: ')
            aggregations.getStreetsByName(cur, subInput)
        case 'si':
            subInput = input('Give me ID of street: ')
            aggregations.getStreetNumbersById(cur, subInput)
        case 'sn':
            subInput = input('Give me name of street: ')
            aggregations.getStreetNumbersByName(cur, subInput)
        case 'q':
            veryImportant.soMuchImportant()
        case _:
            print('\nWhat?!')
    print()
