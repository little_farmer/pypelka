def createTables(cur, conn):
    idColumn = "id INT PRIMARY KEY"
    nameColumn = "name TEXT"
    subDownloadedColumn = "subDownloaded INT"
    parentIdColumn = "parentId INT"

    parentTable = f"{idColumn},{nameColumn},{subDownloadedColumn}"
    middleTable = f"{idColumn},{nameColumn},{parentIdColumn},{subDownloadedColumn}"
    bottomTable = f"{idColumn},{nameColumn},{parentIdColumn}"

    cur.execute(f"CREATE TABLE IF NOT EXISTS region({parentTable});")
    cur.execute(f"CREATE TABLE IF NOT EXISTS district({middleTable});")
    cur.execute(f"CREATE TABLE IF NOT EXISTS city({middleTable});")
    cur.execute(f"CREATE TABLE IF NOT EXISTS cityPart({middleTable});")
    cur.execute(f"CREATE TABLE IF NOT EXISTS street({middleTable});")
    cur.execute(f"CREATE TABLE IF NOT EXISTS number({bottomTable});")
    conn.commit()
