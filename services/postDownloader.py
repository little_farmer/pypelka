import services.cpostApiService as cpostApiService
import services.dbInitService as dbInitService
import time


def postDownloader(cur, conn, entities):
    # check if table exists
    listOfTables = cur.execute(
        'SELECT name FROM sqlite_master  WHERE type="table"; ').fetchall()

    if len(listOfTables) != 6:
        print('Creating database tables')
        dbInitService.createTables(cur, conn)

    def downloadData():
        for entity in entities:
            if entity.get('name') == 'region':
                cpostApiService.fetchAndSaveRegions(cur, conn, entity)
                continue
            cpostApiService.fetchAndSaveBranches(cur, conn, entity, 50)

    def donwloadDataMain():
        try:
            downloadData()
        except:
            time.sleep(10)
            donwloadDataMain()

    donwloadDataMain()
