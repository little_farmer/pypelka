import requests
import progressbar
import concurrent.futures as futures


def fetchAndSaveRegions(cur, conn, entity):
    dbItemsCount = getDbItemsCount(cur, entity.get('name'))
    wanteditemsCount = getDbItemsCountNotDownloaded(cur, entity.get('name'))
    if dbItemsCount != 0 & wanteditemsCount == 0:
        return

    print(f'Starting downloading: {entity.get("name")}')
    regions = requests.get(entity.get("url")).json()
    for region in regions:
        result = cur.execute(
            f"SELECT * FROM region WHERE id = {int(region['id'])} ;").fetchone()
        if result == None:
            regionTuple = (int(region['id']), region['name'], 0)
            cur.execute("INSERT INTO region VALUES(?, ?, ?);", regionTuple)
    conn.commit()

    dbItemsCount = getDbItemsCount(cur, entity.get('name'))
    print(f'Downloaded: {dbItemsCount}\n')


def fetchAndSaveBranches(cur, conn, entity, concurrency):
    wanteditemsCount = getDbItemsCountNotDownloaded(cur, entity.get('parent'))

    if wanteditemsCount == 0:
        return

    print(f'Starting downloading: {entity.get("name")}')
    downloaded = 0
    bar = getBar(wanteditemsCount)

    executor = futures.ThreadPoolExecutor(max_workers=concurrency)

    while True:
        dbItems = getDbItemsNotDownloaded(cur, entity.get('parent'))
        reqs = {executor.submit(requests.get, entity.get('url'), params=[
            (entity.get('searchParameter'), item[0])]): item[0] for item in dbItems}
        for future in futures.as_completed(reqs):
            item = reqs[future]
            newItems = future.result().json()
            for subItem in newItems:
                if entity.get('name') == 'number':
                    insertTuple = (
                        int(subItem['id']), subItem['name'], item)
                    cur.execute(
                        f"INSERT OR IGNORE INTO {entity.get('name')} VALUES(?, ?, ?);", insertTuple)
                else:
                    insertTuple = (
                        int(subItem['id']), subItem['name'], item, 0)
                    cur.execute(
                        f"INSERT OR IGNORE INTO {entity.get('name')} VALUES(?, ?, ?, ?);", insertTuple)
                conn.commit()

            cur.execute(
                f"UPDATE {entity.get('parent')} SET subDownloaded = 1 WHERE id = {str(item)} ;")
            conn.commit()
            downloaded += 1
            bar.update(downloaded)
        dbItems = getDbItemsNotDownloaded(cur, entity.get('parent'))

        if len(dbItems) == 0:
            break

    dbItemsCount = getDbItemsCount(cur, entity.get('name'))
    print(f'\nIterated {entity.get("parent")}: {downloaded}\nDownloaded: {wanteditemsCount}\n{entity.get("name")} in DB: {dbItemsCount}\n')


def getDbItems(cur, table):
    return cur.execute(
        f"SELECT * FROM {table} WHERE subDownloaded = 0;").fetchmany(1000)


def getDbItemsNotDownloaded(cur, table):
    return cur.execute(
        f"SELECT * FROM {table} WHERE subDownloaded = 0;").fetchmany(1000)


def getDbItemsCount(cur, table):
    return cur.execute(
        f"SELECT COUNT(*) FROM {table};").fetchone()[0]


def getDbItemsCountNotDownloaded(cur, table):
    return cur.execute(
        f"SELECT COUNT(*) FROM {table} WHERE subDownloaded = 0;").fetchone()[0]


def getBar(wanteditemsCount):
    widgets = ['[',
               progressbar.Timer(format='%(elapsed)s'),
               '- Downloading ] ',
               progressbar.Bar('*'),
               ' [ ',
               progressbar.Counter(format='%(value)d/%(max_value)d'),
               ' ] (',
               progressbar.ETA(),
               ') ',
               ]

    return progressbar.ProgressBar(
        max_value=wanteditemsCount, widgets=widgets).start()
