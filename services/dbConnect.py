import os
import sys
import sqlite3


def getCurConn(dbName):
    script_dir = os.path.dirname(__file__)
    mymodule_dir = os.path.join(script_dir, '..', 'services')
    sys.path.append(mymodule_dir)

    # Get the current working directory
    cwd = os.getcwd()

    # Construct the relative path to your database file
    db_path = os.path.join(cwd, 'db', dbName+'.db')
    conn = sqlite3.connect(db_path)
    return conn.cursor(), conn
