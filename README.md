# Pypelka

Project for Python course from Pure Storage

## Requirements

-   Python 3.10
-   Installed requirement packages with:

    ```bash
    pip install -r requirements.txt
    ```

## How to run it

```bash
./main.py
```

Default db file is db/post.db

You can change storage with -d flag

## Help

```bash
usage: python main.py [-h] [-d DB]

Pypelka, your personal downloader of cpost

options:
  -h, --help            show this help message and exit
  -d DB, --database DB  Name of database

Use it wisely!
```
