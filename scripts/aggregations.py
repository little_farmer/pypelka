from prettytable import PrettyTable


def countofItem(cur, entities):
    print()
    print('COUNT OF ITEMS')
    counter = 0
    headers = ['Entity', 'Count']
    table = PrettyTable(headers)
    for entity in entities:
        count = getDbItemsCount(cur, entity.get("name"))
        counter += count
        formattedCount = '{:,}'.format(count)
        table.add_row([entity.get('name'), formattedCount])
    formattedCount = '{:,}'.format(counter)
    table.add_row(['SUM', formattedCount])
    table.align['Entity'] = 'l'
    table.align['Count'] = 'r'
    print(table)


def getDbItemsCount(cur, table):
    return cur.execute(
        f"SELECT COUNT(*) FROM {table};").fetchone()[0]


def getStreetsByName(cur, name):
    print()
    print(f'RESULT FOR: {str.upper(name)}')
    address = cur.execute(f"""
    SELECT
        r.name,
        d.name,
        c.name,
        p.name,
        s.name,
        s.id
    FROM street AS s
    INNER JOIN cityPart  AS p    ON p.id = s.parentId
    INNER JOIN city      AS c    ON c.id = p.parentId
    INNER JOIN district  AS d    ON d.id = c.parentId
    INNER JOIN region    AS r    ON r.id = d.parentId
    WHERE LOWER(s.name) = LOWER('{name}');""").fetchall()

    headers = ['Region', 'District', 'City', 'City Part', 'Street', 'ID']
    printTable(headers, address)


def getStreetNumbersByName(cur, name):
    print(f'ALL NUMBERS FOR THIS STREET NAME: {str.upper(name)}')
    address = cur.execute(f"""
    SELECT
        r.name,
        d.name,
        c.name,
        p.name,
        s.name,
        n.name,
        n.id
    FROM street AS s
    INNER JOIN cityPart  AS p    ON p.id = s.parentId
    INNER JOIN city      AS c    ON c.id = p.parentId
    INNER JOIN district  AS d    ON d.id = c.parentId
    INNER JOIN region    AS r    ON r.id = d.parentId
    JOIN number as n ON s.id = n.parentId
    WHERE LOWER(s.name) = LOWER('{name}');""").fetchall()

    headers = ['Region', 'District', 'City',
               'City Part', 'Street', 'Number', 'ID']
    printTable(headers, address)


def getStreetNumbersById(cur, streetId):
    address = cur.execute(f"""
    SELECT
        r.name,
        d.name,
        c.name,
        p.name,
        s.name,
        n.name,
        n.id
    FROM street AS s
    INNER JOIN cityPart  AS p    ON p.id = s.parentId
    INNER JOIN city      AS c    ON c.id = p.parentId
    INNER JOIN district  AS d    ON d.id = c.parentId
    INNER JOIN region    AS r    ON r.id = d.parentId
    JOIN number as n ON s.id = n.parentId
    WHERE s.id = '{streetId}';""").fetchall()

    headers = ['Region', 'District', 'City',
               'City Part', 'Street', 'Number', 'ID']
    printTable(headers, address)


def getMaxEvidenceNumber(cur):
    print()
    print('MAX EVINDENCE NUMBER')
    address = cur.execute(f"""
        SELECT
            r.name,
            d.name,
            c.name,
            p.name,
            s.name,
            s.id,
            n.name,
            n.id
        FROM number as n
        INNER JOIN street    AS s    ON s.id = n.parentId
        INNER JOIN cityPart  AS p    ON p.id = s.parentId
        INNER JOIN city      AS c    ON c.id = p.parentId
        INNER JOIN district  AS d    ON d.id = c.parentId
        INNER JOIN region    AS r    ON r.id = d.parentId
        WHERE CAST(n.name AS INTEGER) IS NOT 9999
        ORDER BY CAST(n.name AS INTEGER) DESC
        LIMIT 15
        """).fetchall()

    headers = ['Region', 'District', 'City', 'City Part',
               'Street', 'Street ID', 'Number', 'ID']
    printTable(headers, address)


def getMaxHouseNumber(cur):
    print()
    print('MAX HOUSE NUMBER')
    address = cur.execute(f"""
    SELECT
        r.name,
        d.name,
        c.name,
        p.name,
        s.name,
        n.name,
        CAST(substr(n.name, 0, instr(n.name, "/")) AS INTEGER) AS houseNumber,
        n.id

    FROM number as n
        INNER JOIN street    AS s    ON s.id = n.parentId
        INNER JOIN cityPart  AS p    ON p.id = s.parentId
        INNER JOIN city      AS c    ON c.id = p.parentId
        INNER JOIN district  AS d    ON d.id = c.parentId
        INNER JOIN region    AS r    ON r.id = d.parentId
        WHERE houseNumber IS NOT 9999
        ORDER BY houseNumber DESC
        LIMIT 15
    """)

    headers = ['Region', 'District', 'City', 'City Part',
               'Street', 'Number', 'House Number',  'ID']
    printTable(headers, address)


def getMaxOrientationNumber(cur):
    print()
    print('MAX ORIENTATION NUMBER')
    address = cur.execute(f"""
    SELECT 
        r.name,
        d.name,
        c.name,
        p.name,
        s.name,
        n.name,
        CAST(substr(n.name, instr(n.name, "/") + 1) AS INTEGER) AS prd,
        n.id

    FROM number as n
    INNER JOIN street    AS s    ON s.id = n.parentId
    INNER JOIN cityPart  AS p    ON p.id = s.parentId
    INNER JOIN city      AS c    ON c.id = p.parentId
    INNER JOIN district  AS d    ON d.id = c.parentId
    INNER JOIN region    AS r    ON r.id = d.parentId
    WHERE n.name LIKE "%/%"
    ORDER BY prd DESC
    LIMIT 15
    """)

    headers = ['Region', 'District', 'City', 'City Part',
               'Street', 'Number', 'Orientation Number',  'ID']
    printTable(headers, address)


def printTable(headers, data):
    table = PrettyTable(headers)
    for row in data:
        table.add_row(row)
    print(table)
